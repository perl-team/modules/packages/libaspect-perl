Source: libaspect-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Angel Abad <angel@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-install-perl
Build-Depends-Indep: libparams-util-perl <!nocheck>,
                     libsub-install-perl <!nocheck>,
                     libsub-uplevel-perl <!nocheck>,
                     libtask-weaken-perl <!nocheck>,
                     libtest-class-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-nowarnings-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libaspect-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libaspect-perl.git
Homepage: https://metacpan.org/release/Aspect
Rules-Requires-Root: no

Package: libaspect-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libparams-util-perl,
         libsub-install-perl,
         libsub-uplevel-perl,
         libtask-weaken-perl
Description: module for Aspect-Oriented Programming in Perl
 Aspect-oriented Programming (AOP) is a programming method developed by Xerox
 PARC and others. The basic idea is that in complex class systems there are
 certain aspects or behaviors that cannot normally be expressed in a coherent,
 concise and precise way. One example of such aspects are design patterns,
 which combine various kinds of classes to produce a common type of behavior.
 Another is logging. For more information, see <URL:http://www.aosd.net>.
 .
 The Perl Aspect module is focused on subroutine matching and wrapping. It
 allows you to select collections of subroutines using a flexible pointcut
 language, and modify their behavior in any way you want.
